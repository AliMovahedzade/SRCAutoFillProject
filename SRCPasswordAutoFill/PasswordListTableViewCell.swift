//
//  PasswordListTableViewCell.swift
//  SRCPasswordAutoFill
//
//  Created by Macbook on AP 13.02.1403.
//

import Foundation
import UIKit

class PasswordListTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    
    @IBOutlet weak var leftIconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    static let identifier = "PasswordAutoFillListCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
