//
//  CredentialProviderViewController.swift
//  SRCPasswordAutoFill
//
//  Created by Macbook on AP 13.02.1403.
//

import AuthenticationServices

class CredentialProviderViewController: ASCredentialProviderViewController {

    /*
     Prepare your UI to list available credentials for the user to choose from. The items in
     'serviceIdentifiers' describe the service the user is logging in to, so your extension can
     prioritize the most relevant credentials in the list.
    */
    
    @IBOutlet weak var passwordListTableView: UITableView!
    
    // MARK: - Variables
    
    var passwordList: [AutoFillPasswordsListModel] = []
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordListTableView.delegate = self
        passwordListTableView.dataSource = self
        
        passwordList = [
            AutoFillPasswordsListModel(title: "Gmail", account: "Ali Movahedzade Gmail", password: "Ali's password for Gmail"),
            AutoFillPasswordsListModel(title: "FaceBook", account: "Ali Movahedzade FaceBook", password: "Ali's password for facebook"),
            AutoFillPasswordsListModel(title: "X", account: "Ali Movahedzade X", password: "Ali's password for X"),
            AutoFillPasswordsListModel(title: "Yahoo", account: "Ali Movahedzade Yahoo", password: "Ali's password for Yahoo"),
            AutoFillPasswordsListModel(title: "SnapChat", account: "Ali Movahedzade SnapChat", password: "Ali's password for SnapChat"),
        ]
        
    }
    
    override func prepareCredentialList(for serviceIdentifiers: [ASCredentialServiceIdentifier]) {
    }

    /*
     Implement this method if your extension supports showing credentials in the QuickType bar.
     When the user selects a credential from your app, this method will be called with the
     ASPasswordCredentialIdentity your app has previously saved to the ASCredentialIdentityStore.
     Provide the password by completing the extension request with the associated ASPasswordCredential.
     If using the credential would require showing custom UI for authenticating the user, cancel
     the request with error code ASExtensionError.userInteractionRequired.

    override func provideCredentialWithoutUserInteraction(for credentialIdentity: ASPasswordCredentialIdentity) {
        let databaseIsUnlocked = true
        if (databaseIsUnlocked) {
            let passwordCredential = ASPasswordCredential(user: "j_appleseed", password: "apple1234")
            self.extensionContext.completeRequest(withSelectedCredential: passwordCredential, completionHandler: nil)
        } else {
            self.extensionContext.cancelRequest(withError: NSError(domain: ASExtensionErrorDomain, code:ASExtensionError.userInteractionRequired.rawValue))
        }
    }
    */

    /*
     Implement this method if provideCredentialWithoutUserInteraction(for:) can fail with
     ASExtensionError.userInteractionRequired. In this case, the system may present your extension's
     UI and call this method. Show appropriate UI for authenticating the user then provide the password
     by completing the extension request with the associated ASPasswordCredential.

    override func prepareInterfaceToProvideCredential(for credentialIdentity: ASPasswordCredentialIdentity) {
    }
    */

    @IBAction func cancel(_ sender: AnyObject?) {
        self.extensionContext.cancelRequest(withError: NSError(domain: ASExtensionErrorDomain, code: ASExtensionError.userCanceled.rawValue))
    }

//    @IBAction func passwordSelected(_ sender: AnyObject?) {
//        let passwordCredential = ASPasswordCredential(user: "j_appleseed", password: "apple1234")
//        self.extensionContext.completeRequest(withSelectedCredential: passwordCredential, completionHandler: nil)
//    }

}


extension CredentialProviderViewController: UITableViewDelegate, UITableViewDataSource {
    
    // UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return passwordList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PasswordListTableViewCell.identifier, for: indexPath) as? PasswordListTableViewCell else {
            fatalError("Something went wrong")
        }
        cell.titleLabel.text = passwordList[indexPath.row].title
        cell.detailLabel.text = passwordList[indexPath.row].account
        cell.leftIconImageView.image = UIImage(systemName: "lock.fill")
        return cell
    }
    
    // UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let passwordCredential = ASPasswordCredential(user: passwordList[indexPath.row].account,
                                                      password: passwordList[indexPath.row].password)
        self.extensionContext.completeRequest(withSelectedCredential: passwordCredential, completionHandler: nil)
    }
}
