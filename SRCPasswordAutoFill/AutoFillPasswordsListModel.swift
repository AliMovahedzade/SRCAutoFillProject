//
//  PasswordListModel.swift
//  SRCPasswordAutoFill
//
//  Created by Macbook on AP 13.02.1403.
//


import Foundation

struct AutoFillPasswordsListModel: Identifiable {
    
    var id = UUID().uuidString
    
    var title: String
    
    var account: String
    
    var password: String
}
